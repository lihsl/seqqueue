/* Sun Aug 12 01:22:58 CST 2018 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MAXSIZE 4

typedef struct
{
	int rear;
	int front;
	int *base;
} queue_t, *queue_ptr;

int init(queue_ptr q);
int enqueue(queue_ptr q,int e);
int dequeue(queue_ptr q,int*e);
int isempty(queue_ptr q);
int getlength(queue_ptr q);

int main(void)
{
	int val;
	queue_t Q;
	assert(init(&Q) == 1);
	assert(getlength(&Q) == 0);

	assert(enqueue(&Q,1) == 1);
	assert(enqueue(&Q,2) == 1);
	assert(enqueue(&Q,3) == 1);
	assert(enqueue(&Q,4) == 0);

	assert(getlength(&Q) == 3);

	assert(dequeue(&Q,&val) == 1 && val == 1);
	assert(dequeue(&Q,&val) == 1 && val == 2);
	assert(dequeue(&Q,&val) == 1 && val == 3);
	assert(dequeue(&Q,&val) == 0);
	assert(getlength(&Q) == 0);

	assert(enqueue(&Q,5) == 1);
	assert(dequeue(&Q,&val) == 1 && val == 5);
	assert(dequeue(&Q,&val) == 0);

	printf("All assertion test passed.\n");

	return 0;
}

int init(queue_ptr q)
{
	q->base = (int*)malloc(sizeof(queue_t) * MAXSIZE);
	if(NULL == q->base) return 0;

	q->front = q->rear = 0;

	return 1;
}

/*
 * 入队列
 * 当前队列没有满【判断队满（rear+1)%MAXSIZE==front】就继续入
 */
int enqueue(queue_ptr q, int e)
{
	if((q->rear+1)%MAXSIZE == q->front) 
		return 0 ;

	q->base[q->rear] = e;
	q->rear = (q->rear+1)%MAXSIZE;

	return 1;
}

/*
 * 出队列 
 * 当前这个队列不为空
 */
int dequeue(queue_ptr q, int *e)
{
	if(q->front == q->rear) return 0;	

	*e = q->base[q->front];
	q->front = (q->front+1) % MAXSIZE;

	return 1;	
}

int getlength(queue_ptr q)
{
	int length = 0;

	length = (q->rear-q->front + MAXSIZE) % MAXSIZE;
	/* printf("length:%d\n",length); */

	return length;
}

